package com.silva021.ioaysapplication.domain.datastore

import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.silva021.ioaysapplication.data.model.UserCredentials
import com.silva021.ioaysapplication.utils.constant.Constant
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException

private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "profile")
class DataStoreRepository(private val context: Context) {
    private object PreferenceKeys {
        val accessToken = stringPreferencesKey(Constant.KEY_ACCESS_TOKEN)
        val client = stringPreferencesKey(Constant.KEY_CLIENT)
        val uid = stringPreferencesKey(Constant.KEY_UID)
    }

    suspend fun saveCredentialsPreferences(user: UserCredentials) {
        context.dataStore.edit { settings ->
            settings[PreferenceKeys.accessToken] = user.accessToken
            settings[PreferenceKeys.uid] = user.uid
            settings[PreferenceKeys.client] = user.client
        }
    }
    val readUserCredentialsDataStore: Flow<UserCredentials> = context.dataStore.data
        .catch { exception ->
            if(exception is IOException){
                Log.d("DataStore", exception.message.toString())
                emit(emptyPreferences())
            }else {
                throw exception
            }
        }
        .map { preference ->
            val accessToken = preference[PreferenceKeys.accessToken] ?: ""
            val uid = preference[PreferenceKeys.uid] ?: ""
            val client = preference[PreferenceKeys.client] ?: ""
            UserCredentials(uid, accessToken, client)
        }

}