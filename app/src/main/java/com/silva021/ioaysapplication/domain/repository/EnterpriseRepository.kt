package com.silva021.ioaysapplication.domain.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.silva021.ioaysapplication.data.api.EnterpriseService
import com.silva021.ioaysapplication.data.api.RetrofitFactory
import com.silva021.ioaysapplication.data.model.EnterpriseList
import com.silva021.ioaysapplication.data.model.LoginResponse
import com.silva021.ioaysapplication.utils.constant.Constant.Companion.TAG_ERROR_ENTERPRISE_REPOSITORY
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EnterpriseRepository {
    fun getEnterprises(map: Map<String?, String?>?): LiveData<EnterpriseList> {
        val data: MutableLiveData<EnterpriseList> = MutableLiveData<EnterpriseList>()
        RetrofitFactory.createService(EnterpriseService::class.java).getListEnterprises(map)
            ?.enqueue(object : Callback<EnterpriseList?> {
                override fun onResponse(
                    call: Call<EnterpriseList?>,
                    response: Response<EnterpriseList?>
                ) {
                    if (response.code() == 200) {
                        data.setValue(response.body())
                    } else if (response.code() == 401 || response.code() == 404) data.setValue(null)
                }

                override fun onFailure(call: Call<EnterpriseList?>, t: Throwable) {
                    Log.e(TAG_ERROR_ENTERPRISE_REPOSITORY, t.message!!)
                }
            })
        return data
    }

    fun getEnterprisesByName(map: Map<String?, String?>, name: String?): LiveData<EnterpriseList> {
        val data: MutableLiveData<EnterpriseList> = MutableLiveData<EnterpriseList>()
        RetrofitFactory.createService(EnterpriseService::class.java)
            .getListEnterprisesByName(map, name)?.enqueue(object : Callback<EnterpriseList?> {
                override fun onResponse(
                    call: Call<EnterpriseList?>,
                    response: Response<EnterpriseList?>
                ) {
                    if (response.code() == 200) {
                        data.setValue(response.body())
                    } else if (response.code() == 401 || response.code() == 404) data.value = null
                }

                override fun onFailure(call: Call<EnterpriseList?>, t: Throwable) {
                    Log.e(TAG_ERROR_ENTERPRISE_REPOSITORY, t.message!!)
                }
            })
        return data
    }


}