package com.silva021.ioaysapplication.domain.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.silva021.ioaysapplication.data.api.LoginService
import com.silva021.ioaysapplication.data.api.RetrofitFactory
import com.silva021.ioaysapplication.data.model.LoginRequest
import com.silva021.ioaysapplication.data.model.LoginResponse
import com.silva021.ioaysapplication.data.model.UserCredentials
import com.silva021.ioaysapplication.utils.constant.Constant.Companion.TAG_ERROR_LOGIN_REPOSITORY
import com.silva021.ioaysapplication.domain.datastore.DataStoreRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginRepository(val context: Context) {

    fun signIn(body: LoginRequest?): LiveData<LoginResponse> {
        val data: MutableLiveData<LoginResponse> = MutableLiveData<LoginResponse>()
        RetrofitFactory.createService(LoginService::class.java).signIn(body)
            ?.enqueue(object : Callback<LoginResponse?> {
                override fun onResponse(
                    call: Call<LoginResponse?>,
                    response: Response<LoginResponse?>
                ) {
                    if (response.code() == 200) {
                        data.value = response.body()
                        saveCredentialsPreferences(response)
                    } else if (response.code() == 401 || response.code() == 404) data.value = null
                }

                override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                    Log.e(TAG_ERROR_LOGIN_REPOSITORY, t.message!!)
                }
            })
        return data
    }

    fun saveCredentialsPreferences(loginResponse: Response<LoginResponse?>) {
        val uid = loginResponse.headers()["uid"]
        val accessToken = loginResponse.headers()["access-token"]
        val client = loginResponse.headers()["client"]

        Log.d(TAG_ERROR_LOGIN_REPOSITORY, "UID $uid acess-token $accessToken client $client")

        CoroutineScope(Dispatchers.IO).launch {
            DataStoreRepository(context).saveCredentialsPreferences(UserCredentials(uid!!, accessToken!!, client!!))
        }
    }

}