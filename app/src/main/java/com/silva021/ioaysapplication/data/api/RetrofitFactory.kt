package com.silva021.ioaysapplication.data.api

import com.silva021.ioaysapplication.utils.constant.Constant
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitFactory {

    companion object {
        private val mBuilder: Retrofit.Builder = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL + Constant.VERSION_API)
            .addConverterFactory(GsonConverterFactory.create())

        private val mRetrofit = mBuilder.build()

        fun <T> createService(classService: Class<T>?): T {
            return mRetrofit.create(classService)
        }
    }

}