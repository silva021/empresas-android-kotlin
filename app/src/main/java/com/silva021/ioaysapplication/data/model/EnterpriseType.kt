package com.silva021.ioaysapplication.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class EnterpriseType(
    @SerializedName("id")
    @Expose
    val id: Int? = null, @SerializedName("enterprise_type_name")
    @Expose
    val enterpriseTypeName: String? = null
) : Serializable