package com.silva021.ioaysapplication.data.api

import com.silva021.ioaysapplication.data.model.LoginResponse

interface LoginCallback  {
    fun onSuccess(data: LoginResponse)
    fun onError(error: String?)
}