package com.silva021.ioaysapplication.data.api

import com.silva021.ioaysapplication.data.model.LoginRequest
import com.silva021.ioaysapplication.data.model.LoginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {
    @POST("users/auth/sign_in")
    fun signIn(@Body loginRequest: LoginRequest?): Call<LoginResponse?>?
}