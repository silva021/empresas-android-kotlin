package com.silva021.ioaysapplication.data.model

import com.silva021.ioaysapplication.utils.constant.Constant

class UserCredentials(var uid: String, var accessToken: String, var client: String) {

    fun getHashMap(): Map<String?, String?> {
        val data: HashMap<String, String> = HashMap()
        data[Constant.KEY_UID] = uid
        data[Constant.KEY_ACCESS_TOKEN] = accessToken
        data[Constant.KEY_CLIENT] = client
        return data.toMap()
    }
}