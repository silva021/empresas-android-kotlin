package com.silva021.ioaysapplication.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Enterprise  (
    @SerializedName("id")
    @Expose
    val id: Int? = null

    ,@SerializedName("email_enterprise")
    @Expose
    val emailEnterprise: String? = null

    ,@SerializedName("facebook")
    @Expose
    val facebook: String? = null

    ,@SerializedName("twitter")
    @Expose
    val twitter: String? = null

    ,@SerializedName("linkedin")
    @Expose
    val linkedin: String? = null

    ,@SerializedName("phone")
    @Expose
    val phone: String? = null

    ,@SerializedName("own_enterprise")
    @Expose
    val ownEnterprise: Boolean? = null

    ,@SerializedName("enterprise_name")
    @Expose
    val enterpriseName: String? = null

    ,@SerializedName("photo")
    @Expose
    val photo: String? = null

    ,@SerializedName("description")
    @Expose
    val description: String? = null

    ,@SerializedName("city")
    @Expose
    val city: String? = null

    ,@SerializedName("country")
    @Expose
    val country: String? = null

    ,@SerializedName("value")
    @Expose
    val value: Int? = null

    ,@SerializedName("share_price")
    @Expose
    val sharePrice: Int? = null

    ,@SerializedName("enterprise_type")
    @Expose
    val enterpriseType: EnterpriseType? = null
) : Serializable