package com.silva021.ioaysapplication.data.model

class Investor {
    var id = 0
    var investor_name: String? = null
    var email: String? = null
    var city: String? = null
    var country: String? = null
    var balance = 0
    var photo: String? = null
    var password: String? = null
    var portfolio: Portfolio? = null
    var portfolio_value = 0
    var isFirst_access = false
    var isSuper_angel = false
}