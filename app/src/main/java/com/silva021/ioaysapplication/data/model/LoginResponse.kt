package com.silva021.ioaysapplication.data.model

class LoginResponse(val investor: Investor, val isSuccess: Boolean, val enterprise: String)