package com.silva021.ioaysapplication.data.api

import com.silva021.ioaysapplication.data.model.EnterpriseList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface EnterpriseService {
    @GET("enterprises")
    fun getListEnterprises(@QueryMap map: Map<String?, String?>?): Call<EnterpriseList?>?

    @GET("enterprises")
    fun getListEnterprisesByName(
        @QueryMap map: Map<String?, String?>?,
        @Query("name") name: String?
    ): Call<EnterpriseList?>?
}