package com.silva021.ioaysapplication.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class EnterpriseList (
    var success: Boolean
    ,@SerializedName("enterprises")
    @Expose
    var enterprise: List<Enterprise?>? = null
) : Serializable