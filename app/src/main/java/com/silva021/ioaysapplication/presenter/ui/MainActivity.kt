package com.silva021.ioaysapplication.presenter.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuItemCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.silva021.ioaysapplication.R
import com.silva021.ioaysapplication.presenter.adapter.EnterpriseAdapter
import com.silva021.ioaysapplication.databinding.ActivityMainBinding
import com.silva021.ioaysapplication.data.model.Enterprise
import com.silva021.ioaysapplication.data.model.UserCredentials
import com.silva021.ioaysapplication.utils.constant.Constant.Companion.isConnectedInternet
import com.silva021.ioaysapplication.domain.datastore.DataStoreRepository
import com.silva021.ioaysapplication.presenter.viewmodel.EnterpriseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(), EnterpriseAdapter.OnItemClickListener {
    private lateinit var mBinding: ActivityMainBinding
    private lateinit var userCredentials: UserCredentials
    private lateinit var enterpriseAdapter: EnterpriseAdapter
    private lateinit var mEnterpriseViewModel: EnterpriseViewModel
    private var mListEnterprises: ArrayList<Enterprise> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setSupportActionBar(mBinding.toolbar)
        Objects.requireNonNull(supportActionBar)?.title = ""


        mEnterpriseViewModel = ViewModelProvider(this).get(EnterpriseViewModel::class.java)

        initializeRecycler()

        DataStoreRepository(applicationContext).readUserCredentialsDataStore.asLiveData()
            .observe(this, { userCredentials -> this.userCredentials = userCredentials })
    }


    private fun initializeRecycler() {
        enterpriseAdapter = EnterpriseAdapter(applicationContext)
        enterpriseAdapter.setData(mListEnterprises)
        enterpriseAdapter.setOnClickListener(this)
        mBinding.recycler.layoutManager = LinearLayoutManager(applicationContext)
        mBinding.recycler.adapter = enterpriseAdapter
    }


    private fun showRecycler() {
        mBinding.recycler.visibility = View.VISIBLE
        mBinding.txtMessageNotFound.visibility = View.GONE
        mBinding.txtMessageInitialize.visibility = View.GONE
    }

    fun showMessage(isNotFound: Boolean) {
        mBinding.recycler.visibility = View.GONE
        mBinding.txtMessageInitialize.visibility = if (isNotFound) View.GONE else View.VISIBLE
        mBinding.txtMessageNotFound.visibility = if (isNotFound) View.VISIBLE else View.GONE
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchView: SearchView =
            MenuItemCompat.getActionView(menu.findItem(R.id.search_view)) as SearchView
        searchView.setOnSearchClickListener { mBinding.layoutLogo.visibility = View.GONE }
        searchView.setOnCloseListener {
            mBinding.layoutLogo.visibility = View.VISIBLE
            showMessage(false)
            false
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (isConnectedInternet(applicationContext)) {
                    mEnterpriseViewModel.getListEnterpriseByName(
                        this@MainActivity.userCredentials,
                        query
                    ).observe(this@MainActivity) { enterprises ->

                        if (enterprises.enterprise?.isNotEmpty() == true)
                            updateRecyclerView(enterprises.enterprise as List<Enterprise>?)
                        else showMessage(enterprises.enterprise!!.isEmpty())
                    }
                    mBinding.layoutLogo.visibility = View.VISIBLE
                } else {
                    showMessageInternet()
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                mBinding.layoutLogo.visibility = View.GONE
                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onItemClick(enterprise: Enterprise?) {
        startActivity(Intent(this, DetailsActivity::class.java).putExtra("company", enterprise))
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top)
    }

    private fun showMessageInternet() {
        CoroutineScope(Dispatchers.Main).launch {
            delay(1000)
            Snackbar.make(
                currentFocus!!,
                "Seu dispositivo não está connectado a nenhuma rede, tente novamente",
                Snackbar.LENGTH_LONG
            ).setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show()
        }
    }

    fun updateRecyclerView(enterpriseList: List<Enterprise>?) {
        enterpriseAdapter.setData(enterpriseList)
        showRecycler()
    }

}