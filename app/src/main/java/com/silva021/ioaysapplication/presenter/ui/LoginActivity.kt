package com.silva021.ioaysapplication.presenter.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.silva021.ioaysapplication.R
import com.silva021.ioaysapplication.databinding.ActivityLoginBinding
import com.silva021.ioaysapplication.data.model.LoginRequest
import com.silva021.ioaysapplication.utils.constant.Constant
import com.silva021.ioaysapplication.utils.dialog.MyProgressDialog
import com.silva021.ioaysapplication.presenter.viewmodel.LoginViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class LoginActivity : AppCompatActivity() {
    var mBinding: ActivityLoginBinding? = null
    var loginViewModel: LoginViewModel? = null
    var dialog: MyProgressDialog = MyProgressDialog(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(mBinding!!.root)

        loginViewModel = ViewModelProvider(this@LoginActivity)[LoginViewModel::class.java]
        mBinding!!.btnLogin.setOnClickListener { signIn() }
    }


    private fun signIn() {
        dialog.showDialog()
        mBinding?.txtMessageError?.visibility = View.GONE
        if (Constant.isConnectedInternet(applicationContext)) {
            loginViewModel?.signIn(returnCredentialsUI())?.observe(this) { loginResponse ->

                if (loginResponse != null)
                    CoroutineScope(Dispatchers.Main).launch {
                        delay(2000)
                        Constant.openActivity(
                            this@LoginActivity,
                            MainActivity::class.java
                        )
                    }
                else showMessageLoginUnauthorized()


            }
        } else {
            showMessageInternet()
        }
    }

    private fun showMessageInternet() {
        dialog.hideDialog()
        Snackbar.make(
            currentFocus!!,
            "Seu dispositivo não está connectado a nenhuma rede, tente novamente",
            Snackbar.LENGTH_LONG
        ).setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show()
    }

    private fun returnCredentialsUI(): LoginRequest? {
        return LoginRequest(
            mBinding?.edtEmail?.text.toString(),
            mBinding?.edtPassword?.text.toString()
        )
    }

    private fun showMessageLoginUnauthorized() {
        dialog.hideDialog()
        mBinding?.txtMessageError?.visibility = View.VISIBLE
        mBinding?.btnLogin?.setBackgroundColor(resources.getColor(R.color.color_button_error))
    }

    override fun onStop() {
        super.onStop()
        if (dialog.dialogIsNull()) {
            dialog.closeDialog()
        }
    }
}