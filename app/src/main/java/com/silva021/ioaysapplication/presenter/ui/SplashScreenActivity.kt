package com.silva021.ioaysapplication.presenter.ui

import android.os.Bundle
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.silva021.ioaysapplication.R
import com.silva021.ioaysapplication.databinding.ActivitySplashScreenBinding
import com.silva021.ioaysapplication.utils.constant.Constant.Companion.openActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashScreenActivity : AppCompatActivity() {
    var mBinding: ActivitySplashScreenBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(mBinding!!.root)

        startAnimation()
        CoroutineScope(Dispatchers.Main).launch {
            delay(4000)
            openActivity(
                this@SplashScreenActivity,
                LoginActivity::class.java
            )
        }
    }


    private fun startAnimation() {
        mBinding?.imgLogo?.startAnimation(
            AnimationUtils.loadAnimation(
                applicationContext,
                R.anim.heart_animation
            )
        )
    }


}