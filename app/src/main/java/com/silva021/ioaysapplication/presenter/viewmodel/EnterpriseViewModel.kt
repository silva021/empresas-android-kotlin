package com.silva021.ioaysapplication.presenter.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.silva021.ioaysapplication.data.model.EnterpriseList
import com.silva021.ioaysapplication.data.model.UserCredentials
import com.silva021.ioaysapplication.domain.repository.EnterpriseRepository


class EnterpriseViewModel() : ViewModel() {
    private val enterpriseRepository: EnterpriseRepository = EnterpriseRepository()
    fun getListEnterpriseByName(userCredentials: UserCredentials, name: String?): LiveData<EnterpriseList> {
        return enterpriseRepository.getEnterprisesByName(
            userCredentials.getHashMap(),
            name
        )
    }

}
