package com.silva021.ioaysapplication.presenter.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.silva021.ioaysapplication.data.model.LoginRequest
import com.silva021.ioaysapplication.data.model.LoginResponse
import com.silva021.ioaysapplication.domain.repository.LoginRepository

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    private val loginRepository: LoginRepository = LoginRepository(application.applicationContext)
    fun signIn(body: LoginRequest?): LiveData<LoginResponse> {
        return loginRepository.signIn(body)
    }

}