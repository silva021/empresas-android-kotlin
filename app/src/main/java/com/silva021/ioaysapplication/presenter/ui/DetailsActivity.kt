package com.silva021.ioaysapplication.presenter.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.silva021.ioaysapplication.R
import com.silva021.ioaysapplication.databinding.ActivityDetailsBinding
import com.silva021.ioaysapplication.data.model.Enterprise
import com.silva021.ioaysapplication.utils.constant.Constant
import java.util.*

class DetailsActivity : AppCompatActivity() {

    private lateinit var mEnterprise: Enterprise
    lateinit var mBinding: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        if (intent.extras != null) {
            mEnterprise = (intent.extras!!.getSerializable("company") as Enterprise?)!!
        }

        initializeComponents()
    }


    private fun initializeComponents() {
        setSupportActionBar(mBinding.toolbar)
        Objects.requireNonNull(supportActionBar)!!.setDisplayHomeAsUpEnabled(true)
        initializeBindingEnterpriseData()
    }

    private fun initializeBindingEnterpriseData() {
        Objects.requireNonNull(supportActionBar)?.title = mEnterprise.enterpriseName
        mBinding.txtDescriptionCompany.text = mEnterprise.description
        Glide.with(applicationContext).load(Constant.BASE_URL + mEnterprise.photo)
//            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .into(mBinding.imgLogoCompany)
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finishActivity()
    }

    private fun finishActivity() {
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finishActivity()
            return true
        }
        return false
    }
}