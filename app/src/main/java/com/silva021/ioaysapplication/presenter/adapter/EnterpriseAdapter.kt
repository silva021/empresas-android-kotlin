package com.silva021.ioaysapplication.presenter.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.silva021.ioaysapplication.databinding.LayoutCompanyBinding
import com.silva021.ioaysapplication.data.model.Enterprise
import com.silva021.ioaysapplication.utils.constant.Constant


class EnterpriseAdapter(var mContext: Context) :
    RecyclerView.Adapter<EnterpriseAdapter.ViewHolder>() {
    private val mEnterpriseList: MutableList<Enterprise> = ArrayList()
    private var mListener: OnItemClickListener? = null
    fun setData(data: List<Enterprise>?) {
        mEnterpriseList.clear()
        mEnterpriseList.addAll(data!!)
        notifyDataSetChanged()
    }

    fun setOnClickListener(mListener: OnItemClickListener?) {
        this.mListener = mListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutCompanyBinding.inflate(
                LayoutInflater.from(mContext),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val company = mEnterpriseList[position]
        holder.mBinding.txtNameCompany.text = company.enterpriseName
        holder.mBinding.txtCountryCompany.text = company.country
        holder.mBinding.txtTypeCompany.text = company.enterpriseType!!.enterpriseTypeName
        if (company.photo!!.isNotEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.mBinding.imgCompany.clipToOutline = true
            }
            Glide.with(mContext).load(Constant.BASE_URL + company.photo)
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(holder.mBinding.imgCompany)
        }
    }

    fun returnCompanyPosition(position: Int): Enterprise {
        return mEnterpriseList[position]
    }

    override fun getItemCount(): Int {
        return mEnterpriseList.size
    }

    inner class ViewHolder(binding: LayoutCompanyBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var mBinding: LayoutCompanyBinding = binding

        init {
            if (mListener != null) mBinding.layoutCard.setOnClickListener {
                mListener!!.onItemClick(
                    returnCompanyPosition(adapterPosition)
                )
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(enterprise: Enterprise?)
    }
}
