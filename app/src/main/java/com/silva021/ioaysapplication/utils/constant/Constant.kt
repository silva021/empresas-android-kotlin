package com.silva021.ioaysapplication.utils.constant

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager

class Constant {

    companion object {
        const val BASE_URL = "https://empresas.ioasys.com.br"
        const val VERSION_API = "/api/v1/"
        const val FILE_PREFERENCES = "user_config"
        const val KEY_ACCESS_TOKEN = "access-token"
        const val KEY_CLIENT = "client"
        const val KEY_UID = "uid"
        const val TAG_ERROR_LOGIN_REPOSITORY = "loginRepository"
        const val TAG_ERROR_ENTERPRISE_REPOSITORY = "mainRepository"

        fun <T> openActivity(activity: Activity, classActivity: Class<T>?) {
            activity.startActivity(Intent(activity, classActivity))
            activity.finish()
        }

        fun isConnectedInternet(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val info = cm.activeNetworkInfo
            return info != null && info.isConnected
        }
    }


}