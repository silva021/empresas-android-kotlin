package com.silva021.ioaysapplication.utils.dialog

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.silva021.ioaysapplication.R

class MyProgressDialog(var activity: Activity) {
    private var alertDialog: AlertDialog? = null
    fun showDialog() {
        alertDialog = AlertDialog.Builder(activity)
            .setView(activity.layoutInflater.inflate(R.layout.progress_dialog, null))
            .setCancelable(false).create()
        alertDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog!!.show()
    }

    fun closeDialog() {
        if (alertDialog != null) {
            if (alertDialog!!.isShowing) {
                alertDialog!!.dismiss()
            }
            alertDialog = null
        }
    }

    fun hideDialog() {
        if (alertDialog != null) {
            alertDialog!!.dismiss()
        }
    }

    fun dialogIsNull(): Boolean = alertDialog == null

}